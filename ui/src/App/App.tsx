import React from "react";
import "./App.scss";
import Navbar from "components/layout/Navbar/Navbar";
import ContentBox from "components/layout/ContentBox";
import ServerPage from "components/pages/ServersPage";

const App: React.FC = () => {

  return (
    <div id="app">
      <Navbar />
      <ContentBox>
        <ServerPage />
      </ContentBox>
    </div>
  );
};

export default App;
