import React from "react";
import ReactDOM from "react-dom";
import App from "./App/App";
import axios from "axios";
axios.defaults.baseURL = "http://localhost:4454";

ReactDOM.render(<App />, document.getElementById("root"));
