import React from "react";
import "./Logo.scss";

const Logo: React.FC = () => {
    return (
      <div className="app-logo">
        <span className="app-logo__circle"></span>
        <span className="app-logo__title">Recruitment task</span>
      </div>
    );
}

export default Logo
