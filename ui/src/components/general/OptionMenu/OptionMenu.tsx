import React, { useState, useRef, useEffect } from "react";
import { OptionMenuProps } from "./";
import "./OptionMenu.scss";
import { useClickOutside } from "hooks/useClickOutside";

const OptionMenu: React.FC<OptionMenuProps> = (props) => {
    const { children, anchorel } = props;
    const [show, setShow] = useState<boolean>(false);
    const optionMenuRef = useRef<HTMLUListElement>(null);
    

    useEffect(() => {
        let achorElement: HTMLButtonElement | undefined;
        if (anchorel.current) {
            achorElement = anchorel.current;
            achorElement.addEventListener("click", toggleMenu);
        }
        return () => {
            if (achorElement) {
              achorElement.removeEventListener("click", toggleMenu);
            }
      };
    }, [anchorel]);
    
    const toggleMenu = (): void => setShow(state => !state);
    const hideMenu = (): void => {
       setShow(false);
    }

    useClickOutside(optionMenuRef, anchorel, hideMenu);

    return show ? (
      <ul {...props} ref={optionMenuRef} className="option-menu" onClick={hideMenu}>
        {children}
      </ul>
    ) : null;
}

export default OptionMenu
