import React from "react";
import "./MenuItem.scss";
import { MenuItemProps } from "./";

const MenuItem: React.FC<MenuItemProps> = (props) => {
    return (
      <li {...props} className={`menu-item ${props.className || ""}`}>
        {props.children}
      </li>
    );
};

export default MenuItem
