import MenuItem from "./MenuItem";

export interface MenuItemProps extends React.ComponentProps<"li"> { };

export default MenuItem;