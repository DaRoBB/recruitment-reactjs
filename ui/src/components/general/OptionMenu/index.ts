import React from "react";
import OptionMenu from "./OptionMenu";

export interface OptionMenuProps extends React.ComponentProps<"ul"> {
  anchorel: React.RefObject<HTMLButtonElement>;
}

export default OptionMenu;