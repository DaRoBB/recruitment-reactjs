import React from "react";
import "./ServerTable.scss";
import ServerElement, { ServerProps } from "./ServerElement";

interface ServerTableProps {
  servers: ServerProps[];
}

const ServerTable: React.FC<ServerTableProps> = ({ servers }) => {
  return (
    <table className="server-table">
      <thead>
        <tr>
          <th>NAME</th>
          <th>STATUS</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {servers.map((serverProps) => (
          <ServerElement key={serverProps.id} {...serverProps} />
        ))}
      </tbody>
    </table>
  );
};

export default ServerTable
