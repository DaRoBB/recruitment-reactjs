import ServerElement from "./ServerElement";
import { ServerStatusType } from "../ServerStatus";

export interface ServerProps {
  id: number;
  name: string;
  status: ServerStatusType;
}

export default ServerElement;