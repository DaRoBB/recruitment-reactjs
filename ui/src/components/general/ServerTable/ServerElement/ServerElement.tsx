import React, { useRef, useState } from "react";
import "./ServerElement.scss";
import { ServerProps } from ".";
import ServerStatus, { ServerStatusType } from "../ServerStatus";
import IconButton from "components/general/IconButton";
import { ReactComponent as ThreeDotsIcon } from "assets/icons/three-dots-icon.svg";
import OptionMenu from "components/general/OptionMenu";
import MenuItem from "components/general/OptionMenu/MenuItem";
import axios, { AxiosResponse } from "axios";

const ServerElement: React.FC<ServerProps> = ({ id, name, status }) => {
  const menuAnchorEl = useRef<HTMLButtonElement>(null);
  const [serverState, setServerState] = useState<ServerStatusType>(status);

    const trurnServerOn = async () => {
      try {
        const res: AxiosResponse<ServerProps> = await axios.put(`/servers/${id}/on`);
        setServerState(res.data.status);
      } catch (error) {}
  }
  
    const trurnServerOff = async () => {
      try {
        const res: AxiosResponse<ServerProps> = await axios.put(`/servers/${id}/off`);
        setServerState(res.data.status);
      } catch (error) {}
    }

    const rebootServer = async () => {
      try {
        const res: AxiosResponse<ServerProps> = await axios.put(`/servers/${id}/reboot`);
        setServerState(res.data.status);
        const pingServerStatus = setInterval(async () => {
          try {
            const res = await axios.get(`/servers/${id}`);
            if (res.status === 200 && res.data.status !== "REBOOTING") {
              clearInterval(pingServerStatus);
              setServerState(res.data.status);
            }
          } catch (error) {}
        }, 1000);
      } catch (error) {}
    }
    return (
      <tr className="server-element">
        <td className="server-element__name">{name}</td>
        <td className="server-element__status">
          <ServerStatus status={serverState} />
        </td>
        <td className="server-element__option">
          <IconButton ref={menuAnchorEl} icon={<ThreeDotsIcon />} />
          <OptionMenu anchorel={menuAnchorEl}>
            {serverState === "OFFLINE" && <MenuItem onClick={trurnServerOn}>Turn on</MenuItem>}
            {serverState === "ONLINE" && <MenuItem onClick={trurnServerOff}>Turn off</MenuItem>}
            {serverState === "ONLINE" && <MenuItem onClick={rebootServer}>Reboot</MenuItem>}
          </OptionMenu>
        </td>
      </tr>
    );
}

export default ServerElement
