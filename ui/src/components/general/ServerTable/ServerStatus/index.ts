import ServerStatus from "./ServerStatus";

export type ServerStatusType = "ONLINE" | "OFFLINE" | "REBOOTING";

export default ServerStatus;