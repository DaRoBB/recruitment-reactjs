import React from "react";
import "./ServerStatus.scss";
import { ServerStatusType } from "./";
import { ReactComponent as XIcon } from "assets/icons/x-icon.svg";
import { ReactComponent as CircleIcon } from "assets/icons/circle-icon.svg";

const ServerStatus: React.FC<{ status: ServerStatusType }> = ({ status }) => {
    const statusIcon = () => {
        const iconClasses = `server-status__icon server-status--${status}__icon`;
        switch (status) {
          case "ONLINE":
                return <CircleIcon className={iconClasses} />;
          case "OFFLINE":
            return <XIcon className={iconClasses} />;
          default:
            return null;
        }
    }

    return (
      <div className={`server-status server-status--${status}`}>
        {statusIcon()}
        <span className={`server-status__title server-status--${status}__title`}>
          {status}
          {status === "REBOOTING" && "..."}
        </span>
      </div>
    );
};

export default ServerStatus
