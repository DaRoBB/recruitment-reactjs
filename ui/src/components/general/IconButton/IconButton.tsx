import React, { forwardRef } from "react";
import { IconButtonProps } from "./";
import "./IconButton.scss";

const IconButton = forwardRef<HTMLButtonElement, IconButtonProps>((props, ref) => {
    return (
      <button className={`icon-btn ${props.className || ""}`} {...props} ref={ref}>
        <span className="icon-btn__icon">{props.icon}</span>
      </button>
    );
});

export default IconButton;
