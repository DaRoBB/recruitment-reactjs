import IconButton from "./IconButton";

export interface IconButtonProps extends React.ComponentProps<"button"> {
  icon: React.ReactNode;
}

export default IconButton;