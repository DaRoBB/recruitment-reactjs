import React, { ChangeEvent } from "react";
import "./Searchbar.scss";
import { ReactComponent as SearchIcon } from "assets/icons/search-icon.svg";
import { SearchbarProps } from "./";

const Searchbar: React.FC<SearchbarProps> = ({ cb }) => {
  let watingTimeout: ReturnType<typeof setTimeout> | null = null;

  const inputOnChangeDebouncedHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const searchString = e.target.value;
    if (watingTimeout) clearTimeout(watingTimeout);
    watingTimeout = setTimeout(() => cb(searchString), 500);
  };
  return (
    <div className="search-bar">
      <SearchIcon className="search-bar__icon" />
      <input
        className="search-bar__input"
        onChange={inputOnChangeDebouncedHandler}
        type="text"
        placeholder="Search"
      />
    </div>
  );
};

export default Searchbar
