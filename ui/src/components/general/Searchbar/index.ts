import Searchbar from "./Searchbar";

export interface SearchbarProps {
  cb: (val: string) => void;
}

export default Searchbar;