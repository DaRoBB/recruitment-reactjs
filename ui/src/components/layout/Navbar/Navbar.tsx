import React from "react";
import Logo from "components/general/Logo";
import "./Navbar.scss";

const Navbar: React.FC = () => {
    return (
     <nav id="app-navbar"><Logo /></nav>
    )
}

export default Navbar
