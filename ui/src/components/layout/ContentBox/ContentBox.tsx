import React from "react";
import "./ContentBox.scss";

const ContentBox: React.FC = ({ children }) => {
    return <section className="content-box">{children}</section>;
}

export default ContentBox
