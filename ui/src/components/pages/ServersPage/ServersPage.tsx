import React, { useState, useEffect } from "react";
import { ServerProps } from "components/general/ServerTable/ServerElement";
import ServerTable from "components/general/ServerTable";
import Searchbar from "components/general/Searchbar";
import "./ServerPage.scss";
import axios, { AxiosResponse } from "axios";

const ServersPage: React.FC = () => {

    const [servers, setServers] = useState<ServerProps[]>([]);
    const [dislpayServers, setDisplayServers] = useState<ServerProps[]>([]);
    
    useEffect(() => {
        const getAllServers = async () => {
            try {
                const res: AxiosResponse<ServerProps[]> = await axios.get("/servers");
                setServers(res.data);
                setDisplayServers(res.data);
            } catch (error) {}
        }
        getAllServers();
        return () => {}
    }, [])

      const serverSearchHandler = (val: string): void => {
        if (val === "") {
          return setDisplayServers(servers);
        }
        setDisplayServers(servers.filter((server) => server.name.includes(val)));
      };
    return (
      <section className="servers-page">
        <header className="server-header">
          <div className="server-info">
            <h1>Servers</h1>
            <p>Number of elements: {dislpayServers.length}</p>
          </div>
          <Searchbar cb={serverSearchHandler} />
        </header>
        <ServerTable servers={dislpayServers} />
      </section>
    );
}

export default ServersPage
